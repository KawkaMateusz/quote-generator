var gulp = require('gulp');
var pug = require('gulp-pug');
var babel = require('gulp-babel')

gulp.task('pug', function buildHTML() {
  return gulp.src('src/index.pug')
  .pipe(pug()).pipe(gulp.dest('./'));
});

gulp.task('babel', () =>
    gulp.src('src/scripts/main.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist'))
);