(function() {

    // handle 'new quote' button && main container
    const button = document.querySelector('#generate'),
        main = document.querySelector('.main');

    let exist = false;

    // fetch  && generate quote
    const getQuote = (url) => {
        fetch(url)
            .then(res => res.json())
            .then((out) => {
                generateQuote(out.quote, out.cat, out.author)
            })
            .catch(err => {
                throw err
            });
    }

    // create paragraph for quote data
    const createParagraph = (className, txt) => {
        let paragraph = document.createElement('p');
        paragraph.setAttribute('class', className);
        paragraph.appendChild(document.createTextNode(txt));
        return paragraph;
    }

    const generateQuote = (quo, cat, auth) => {
        // If quote exist, delete it before generating a new quote
        if (exist) {
            let article = document.querySelector('.article');
            article.remove()
        }

        // Create article for quotes
        let article = document.createElement('article');
        article.setAttribute('class', 'article')

        // Create paragraphs with data from api
        let quote = createParagraph('quote', quo);
        let category = createParagraph('category', cat);
        let author = createParagraph('author', auth)

        // Add paragraph's to article
        article.appendChild(quote);
        article.appendChild(category);
        article.appendChild(author);

        // Put article to main container
        main.appendChild(article)

        // Change quote exist to true;
        exist = true
    };

    button.addEventListener('click', () => getQuote('https://talaikis.com/api/quotes/random/'), false);
})();