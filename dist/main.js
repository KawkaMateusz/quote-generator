'use strict';

(function () {

    // handle 'new quote' button && main container
    var button = document.querySelector('#generate'),
        main = document.querySelector('.main');

    var exist = false;

    // fetch  && generate quote
    var getQuote = function getQuote(url) {
        fetch(url).then(function (res) {
            return res.json();
        }).then(function (out) {
            generateQuote(out.quote, out.cat, out.author);
        }).catch(function (err) {
            throw err;
        });
    };

    // create paragraph for quote data
    var createParagraph = function createParagraph(className, txt) {
        var paragraph = document.createElement('p');
        paragraph.setAttribute('class', className);
        paragraph.appendChild(document.createTextNode(txt));
        return paragraph;
    };

    var generateQuote = function generateQuote(quo, cat, auth) {
        // If quote exist, delete it before generating a new quote
        if (exist) {
            var _article = document.querySelector('.article');
            _article.remove();
        }

        // Create article for quotes
        var article = document.createElement('article');
        article.setAttribute('class', 'article');

        // Create paragraphs with data from api
        var quote = createParagraph('quote', quo);
        var category = createParagraph('category', cat);
        var author = createParagraph('author', auth);

        // Add paragraph's to article
        article.appendChild(quote);
        article.appendChild(category);
        article.appendChild(author);

        // Put article to main container
        main.appendChild(article);

        // Change quote exist to true;
        exist = true;
    };

    button.addEventListener('click', function () {
        return getQuote('https://talaikis.com/api/quotes/random/');
    }, false);
})();